# Universidad Distrital Francisco José de Caldas
## Facultad Ingeniería

### Capitulo Estudiantil ACM - GIWEB - FRONT-END

#### Laboratorio 3 - Tarea

#### Descripcion
Taller dado con el objetivo de comprender todas las caracteristicas vistas, este debe contener dos paginas enlazadas una especializada en darle la explicacion a lo que esta haciendo en ACM y la otra explicando sus gustos e intereses, tratar de hacer muy vistosa la interfaz, asi con ello se puede llamar la atencion de la gente

#### Creador
Gabriel Vargas Monroy

#### Pagina WEB
[Pagina del Laboratorio 3](http://vmgabriel.gitlab.io/giweb-laboratorio-3/ "Laboratorio GIWEB ACM")

#### Licencia
GNU GPL 3.0
